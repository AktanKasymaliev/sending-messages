import pytz

from django.db import models
from django.contrib.auth.models import  AbstractBaseUser, PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models

from .manager import UserManager

STATUS = [
    ('sent', "Отправлено"), 
    ('await', "В ожидании")
    ]

TAG = [
    ('sent', "Отправлено"), 
    ('await', "В ожидании")
    ]


class User(AbstractBaseUser, PermissionsMixin):

    first_name = models.CharField(max_length=15, blank=True, null=True)
    last_name = models.CharField(max_length=15, blank=True, null=True)
    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField( unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Пример телефоного формата: '+996123123123'")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, unique=True, blank=True)
    phone_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=100, choices=TAG, default='await')
    timezone = models.CharField(max_length=32, choices=TIMEZONES, 
    default='UTC')

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.username

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    class Meta:
        db_table = 'auth_user'
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

class Mailing(models.Model):
    send_time = models.DateTimeField(verbose_name='Дата и время рассылки')
    text = models.TextField(verbose_name='Текст для рассылки')
    
    end_at = models.DateTimeField(verbose_name='Дата окончания рассылки')

    class Meta:
        db_table = 'mailing'
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

class Message(models.Model):
    created_at = models.DateTimeField(verbose_name="Дата и время создания", auto_now_add=True)
    status = models.CharField(verbose_name="Статус", max_length=25, choices=STATUS, default='await')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages_mailings')
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='messages_users')
    
    class Meta:
        db_table = 'messages'
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'