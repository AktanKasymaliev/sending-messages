from django.urls import include, path

from .views.auth_view import RegisterView, CRUDUserView, UserListView

urlpatterns = [
    #user management
    path('users/', UserListView.as_view()), #user list
    path('reg/', RegisterView.as_view()), #registration
    path('retrieve-user/<int:pk>/', CRUDUserView.as_view()), #CRUD user

    
]
