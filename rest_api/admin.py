from django.contrib import admin

from django.contrib.auth import get_user_model

from rest_api.models import Mailing, Message

User = get_user_model()

admin.site.register(User)
admin.site.register(Mailing)
admin.site.register(Message)