from django.contrib.auth import get_user_model

from rest_framework import serializers

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email', 
            "phone_number",
            "phone_code",
            "timezone")



class CRUDUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=6, required=True, write_only=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email', 
            'password',
            "phone_number",
            "phone_code",
            "timezone")

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)

        instance.save()

        return instance

    def create(self, validated_data):    
        user = User.objects.create_user(**validated_data)
        return user

